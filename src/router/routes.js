
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue')
  },
  {
    path: '/solutionMars',
    component: () => import('layouts/Mars.vue')
  },
  {
    path: '/solutionAirline',
    component: () => import('layouts/Airline.vue')
  },
  {
    path: '/contactForm',
    component: () => import('layouts/ContactForm.vue')
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
